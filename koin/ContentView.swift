//
//  ContentView.swift
//  koin
//
//  Created by 정태훈 on 11/6/23.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        
        ZStack(alignment: .top) {
            Color(red: 23 / 255, green: 92 / 255, blue: 142 / 255)
                .ignoresSafeArea(edges: .top)
            MyWebView(urlToLoad: "https://koreatech.in/")
        }
        
    }
}

#Preview {
    ContentView()
}
